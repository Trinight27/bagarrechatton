<?php
/**
 * Programme des chatons trop mignons :)
 * Les chatons ont un type qui régit l'issue de leurs combats.
 * Les chatons eau gagnent contre les chatons feu,
 * qui battent les chatons terre, ces derniers l'emportant face aux chatons eau ;
 * et si deux chatons ont le même type, le combat se solde par une égalité.
 *
 * @author : prof
 * @version : 1.0
 */


$lesPouvoirs = array("feu", "eau", "terre");
$lesChatons["Isidore"] = "feu";
function afficherMenu()
{
    print("----------------------------------------\n");
    print("Menu:\n");
    print("1 - Afficher la liste des chatons \n");
    print("2 - Créer un nouveau chaton \n");
    print("3 - Supprimer un chaton \n");
    print("4 - Lancer un combat \n");
    print("9 - Quitter \n");
    print("----------------------------------------\n");
}

function afficheChaton(array $lesChatons)
{
    print("Liste des chatons : \n");
    foreach ($lesChatons as $nomchaton => $pouvoir) {
        print("Nom : ".$nomchaton."  Pouvoir : ".$pouvoir."\n");
    }
}

function creeChaton(array &$lesPouvoirs,array &$lesChatons)
{
    $nom = (string)readline("Donne lui un nom : "."\n");
    print("Choisir le pouvoir du nouveau chaton :"."\n");
    foreach ($lesPouvoirs as $key => $pouvoir){
        print(($key+1)." ".$pouvoir."\n");
    }
    $nouvPouvoir=(int)readline("");
    $lesChatons[$nom]=$lesPouvoirs[$nouvPouvoir-1];
}

function suprChaton(array &$lesChatons)
{
    $cible = (string)readline("Quelle chaton veut-tu supprimer ? ");
    unset($lesChatons[$cible]);
}

function bagarreChaton(array &$lesChatons)
{
    print(afficheChaton($lesChatons));
    $combattant1=(string)readline("Nom du 1er combattant ? \n");
    $combattant2=(string)readline("Nom du 2eme combattant ? \n");
    if ($combattant1 != $combattant2){
        if (($lesChatons[$combattant1]=="feu" and $lesChatons[$combattant2]=="terre" )or($lesChatons[$combattant1]=="eau" and $lesChatons[$combattant2]=="feu")or($lesChatons[$combattant1]=="terre" and $lesChatons[$combattant2]=="eau")){
            $win=$combattant1." vainqueur !\n";
            unset($lesChatons[$combattant2]);
        }
        else if($lesChatons[$combattant1] == $lesChatons[$combattant2]) {
            $win="Il n'y a pas de vainqueur, ils ont le même pouvoir .\n";

        }
        else{
            $win=$combattant2." vainqueur !\n";
            unset($lesChatons[$combattant1]);
        }
    }
    else{
        print("Le chaton ne peut pas ce battre tout seul \n");
    }
    return "Combat : ".$combattant1." versus ".$combattant2." : ".$win;
}

function choixJoueur(): int
{
    print("Votre choix ?");
    $choix = (int)fgets(STDIN);
    while ($choix != 1 and $choix != 2 and $choix != 3 and $choix != 4 and $choix != 9) {
        afficherMenu();
        print("Votre choix ?");
        $choix = (int)fgets(STDIN);
    }
    return $choix;
}


//Programme de chatons tout mignons
print("Bienvenue dans le programme des Chatons tout mignons :)\n");

$fin = false;

while (!$fin) {

    afficherMenu();
    $choix = choixJoueur();

    //traitement du choix du joueur
    switch ($choix) {
        case 1:
            print(afficheChaton( $lesChatons, $lesPouvoirs));
            // TODO 2 afficher les chatons
            break;
        case 2 :
            print(creeChaton($lesPouvoirs,$lesChatons));
            // TODO 1 créer un chaton
            break;
        case 3 :
            print(suprChaton($lesChatons));
            // TODO 4 supprimer un chaton
            break;
        case 4 :
            print(bagarreChaton($lesChatons));
            //TODO 3 créer un combat de chaton
            break;
        case 9:
            $fin = true;
            break;
        default:
            print('Valeur de choix impossible');
    };
}
